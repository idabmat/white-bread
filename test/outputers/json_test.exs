defmodule WhiteBread.Outputers.JSONTests do
  use ExUnit.Case
  alias WhiteBread.Outputers.JSON

  test "file path fetched on initialization" do
    old = Application.fetch_env!(:white_bread, :outputers)
    :ok = Application.put_env(:white_bread, :outputers, [{JSON, path: "/fu/bar.baz"}])
    assert {:ok, %JSON{path: "/fu/bar.baz"}} = JSON.init([])
    :ok = Application.put_env(:white_bread, :outputers, old)
  end

  test "write file on termination with Poison" do
    :ok  = Application.put_env(:white_bread, :json_library, Poison)
    p = Path.expand("~/fu/report.json")
    JSON.terminate(:normal, %JSON{path: p, data: [%{id: "feature-id"}]})
    assert File.read!(p) == "[\n  {\n    \"id\": \"feature-id\"\n  }\n]"
    File.rm!(p)
    Application.delete_env(:white_bread, :json_library)
  end

  test "write file on termination with Jason" do
    :ok  = Application.put_env(:white_bread, :json_library, Jason)
    p = Path.expand("~/fu/report.json")
    JSON.terminate(:normal, %JSON{path: p, data: [%{id: "feature-id"}]})
    assert File.read!(p) == "[\n  {\n    \"id\": \"feature-id\"\n  }\n]"
    File.rm!(p)
    Application.delete_env(:white_bread, :json_library)
  end
end
