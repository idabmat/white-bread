defmodule WhiteBread.Application do
  @moduledoc false

  use Application

  def start(_, _) do
    Supervisor.start_link(WhiteBread.Supervisor, strategy: :one_for_one)
  end

  def stop() do
    WhiteBread.EventManager.stop()
  end
end
