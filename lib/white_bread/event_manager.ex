defmodule WhiteBread.EventManager do
  @moduledoc false

  ## An Event Manager inspired by that in ExUnit but adapted for
  ## WhiteBread.
  use DynamicSupervisor

  @impl DynamicSupervisor
  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_link(_opts) do
    DynamicSupervisor.start_link(__MODULE__, :ok,  name: __MODULE__)
  end

  def stop() do
    for {_, pid, _, _} <- DynamicSupervisor.which_children(__MODULE__) do
      :ok = DynamicSupervisor.terminate_child(__MODULE__, pid)
    end

    DynamicSupervisor.stop(__MODULE__)
  end

  def add_handler(handler, opts) do
    child_spec = %{id: GenServer, start: {GenServer, :start_link, [handler, opts]}}
    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  def report(details) do
    notify(details)
  end

  defp notify(msg) do
    for {_, pid, _, _} <- DynamicSupervisor.which_children(__MODULE__) do
      GenServer.cast(pid, msg)
    end

    :ok
  end
end
