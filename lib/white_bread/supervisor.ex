defmodule WhiteBread.Supervisor do
  @moduledoc false

  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok, [])
  end

  @impl true
  def init(init_args) do
    Supervisor.init(children(), init_args)
  end

  defp children do
    [WhiteBread.EventManager]
  end
end
